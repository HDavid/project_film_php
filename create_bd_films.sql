drop table ACTEURS cascade constraints;
drop table REALISATEUR cascade constraints;
drop table JOUE_DANS cascade constraints;
drop table FILM cascade constraints;

create table FILM
(
    idF number(3),
    titre Varchar2(50),
    année number(4),
    genre Varchar2(15),
    realis number(3),
    constraint KFilm primary key (idF),
    constraint KRealis foreign key (realis) references REALISATEUR(idReal)
);

create table ACTEURS
(
    nom Varchar2(20),
    prenom Varchar2(20),
    dateNaissance date,
    idAct number(3),
    nationalite Varchar2(20),
    constraint KAct primary key (idAct)
);

create table JOUE_DANS
(
    idFilm number(3),
    idActeur number(3),
    constraint KIdF foreign key (idFilm) references FILM(idF),
    constraint KActeur foreign key (idActeur) references ACTEURS(idAct)
);


create table REALISATEUR
(
    idReal number(3),
    nom Varchar2(20),
    prenom Varchar2(20),
    dateNaissance date,
    nationalite Varchar2(20)
    constraint KReal primary key (idReal)
);