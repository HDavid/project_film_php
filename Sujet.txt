On veut réaliser une application de gestion d’une collection de films en PHP. Cette application ne demande pasd’authentification mais doit permettre de :
—  Consulter la liste des films disponibles selon différents critères
—  Gérer les titres, réalisateurs, années de réalisation et genre (policier, comédie, dessin animé, etc.)
—  Ajouter un film
—  Supprimer un film
On considérera qu’un film a un seul réalisateur et un seul genre. Nourrissez votre base avec une liste de film récupérés sur le Web. Inutile de prendre une base trop importante. 
Options : gérer la modification des films et les affiches de films, qu’on récupérera éventuellement aussi sur le Web. 
Examinez de près cette demande et donnez en une analyse et une implémentation SQL pour commencer.



— Faites l’analyse, créez le sql correspondant
— Quelles sont les pages que vous voulez réaliser et leur rendu approximatif (maquettes) ?
—  Codez en PHP. Privilégiez les pages permettant d’avoir sous la main la plupart des fonctionnalités de l’application de manière synthétique.

Vous devrez fournir au final 2 URLs pour le rendu de votre projet :
—  URL de votre dépôt gitlab en ayant pris soin d’autoriser votre enseignant de TP à y accéder
—  URL de votre site installé à l’IUT du style http://localhost/étud1/projet_PHP_etud1_etud2/

Datelimite du rendu : Le 19 Octobre 2018. Soutenances pendant la dernière séance de TD/TP