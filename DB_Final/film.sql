--
-- Structure de la table `FILM`
--

DROP TABLE IF EXISTS `FILM`;
CREATE TABLE IF NOT EXISTS `FILM` (
  `code_film` int(11) NOT NULL,
  `titre_original` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `synopsis` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `pays` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `annee` int(11) DEFAULT NULL,
  `duree` int(11) DEFAULT NULL, --en minutes
  `affiche` varchar(100) DEFAULT NULL
  `genre` varchar(20) NOT NULL,
) ENGINE=InnoDB AUTO_INCREMENT=569 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `films`
--

INSERT INTO `films` (`code_film`, `titre_original`, `synopsis`, `pays`, `annee`, `duree`,`affiche`) VALUES
(1,'Avengers: Endgame','Thanos ayant anéanti la moitié de l’univers, les Avengers restants resserrent les rangs dans ce vingt-deuxième film des Studios Marvel, grande conclusion d’un des chapitres de l’Univers Cinématographique Marvel.','Amerique',2019,181,'http://fr.web.img3.acsta.net/c_215_290/pictures/19/04/04/09/04/0472053.jpg','action'),
(2,'Joker','Le film, qui relate une histoire originale inédite sur grand écran, se focalise sur la figure emblématique de l’ennemi juré de Batman. Il brosse le portrait d’Arthur Fleck, un homme sans concession méprisé par la société. ','Amérique',2019,122,'http://fr.web.img6.acsta.net/c_215_290/pictures/19/09/03/12/02/4765874.jpg'),
(3,'Hunger Games',"Chaque année, dans les ruines de ce qui était autrefois l'Amérique du Nord, le Capitole, l'impitoyable capitale de la nation de Panem, oblige chacun de ses douze districts à envoyer un garçon et une fille - les 'Tributs' - concourir aux Hunger Games. Les Hunger Games sont un événement télévisé national au cours duquel les tributs doivent s'affronter jusqu'à la mort. L'unique survivant est déclaré vainqueur.
La jeune Katniss, 16 ans, se porte volontaire pour prendre la place de sa jeune sœur dans la compétition. Elle a pour seuls atouts son instinct et un mentor, Haymitch Abernathy. Pour espérer pouvoir revenir un jour chez elle, Katniss va devoir, une fois dans l'arène, faire des choix impossibles entre la survie et son humanité, entre la vie et l'amour...",'Amérique',2012,142,'http://fr.web.img6.acsta.net/c_215_290/medias/nmedia/18/85/51/91/20018884.jpg'),
(4,'Le Dictateur','Dans le ghetto juif vit un petit barbier qui ressemble énormément à Adenoid Hynkel, le dictateur de Tomania qui a décidé l’extermination du peuple juif. Au cours d’une rafle, le barbier est arrêté en compagnie de Schultz, un farouche adversaire d’Hynkel... ','Amérique',1945,125);