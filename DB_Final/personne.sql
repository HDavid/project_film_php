--Structure de la table `individus`
--

DROP TABLE IF EXISTS `PERSONNE`;
CREATE TABLE IF NOT EXISTS `PERSONNE` (
  `code_pres` int(11) NOT NULL,
  `nom` varchar(20) DEFAULT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `nationalite` varchar(20) DEFAULT NULL,
  `date_naiss` int(11) DEFAULT NULL,
  `date_mort` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=892 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PERSONNE`

INSERT INTO `PERSONNE` (`code_pers`, `nom`, `prenom`, `nationalite`, `date_naiss`, `date_mort`) VALUES
(1,'De Niro','Robert', 'Américaine',1943,0),
(2,'Downey Jr','Robert','Américaine',1965,0),
(3,'Lawrence','Jennifer','Américaine',1990,0),
(4,'Chalie','Chaplin','Britannique',1889,1977);