<?php
require_once('connexion.php');

$connexion = connect_bd();
$errorMessage = '';
// Les 2 identifiants sont-ils transmis ?
if(!empty($_POST['login']) && !empty($_POST['password']))
{
 // Est-ce que l'utilisateur est dans la base ?
 $sql="SELECT login,password FROM USERS where login=:login and password=:pass";	
 $stmt = $connexion->prepare($sql);
 $stmt->bindParam(':login', $_POST['login']);
 $stmt->bindParam(':pass', $_POST['password']);
 $stmt->execute();
 if($stmt->rowCount() > 0)
 	{
		// On ouvre la session
		session_start();
		// On enregistre le login en variable de session
		$_SESSION['login'] = $_POST['login'];
		echo "Bravo ".$_POST['login'];
		// On redirige vers le fichier suite.php
		header('Location: suite.php');
	}
 else{
 	$errorMessage = ' Mauvais identifiants !';
 }
}
?>

<!doctype html>
<html lang="fr">
	<head>
		<title>Formulaire authentification</title>
	</head>
	<body>
		<form name="f1" action="authentBD_PDO.php" method="POST">
			<fieldset>
				<legend>Identifiez-vous</legend>
				<p>
					<label for="login">Login :</label>
					<input type="text" id="login" name="login"/>
				</p>
				<p>
					<label for="password">Password :</label>
					<input type="password" id="password" name="password"/>
					<input type="submit" value="Se logguer" />
				</p>
			</fieldset>
		</form>
	</body>
</html>